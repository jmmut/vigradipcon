# Vigradipcon

### Neuron cyclic ponderated directed graph visualizer

This is a small project meant to understand at low level some neural nets. It doesn't center on NN performance, but 
arquitecture and visualization of the values involved. I'm using vigradipcon mainly for unsupervised nets and temporal 
patterns.


## Example: Hopfield network

![vigradipcon-color.png](https://www.dropbox.com/s/z3rkoni4v8v3hue/vigradipcon-color.png?dl=1)

The net you see in the image is a Hopfield net, which is an [autoassociative memory](https://en.wikipedia.org/wiki/Autoassociative_memory).
In the image you can see in the bottom left some patterns available as input for training, in the upper left there 
are the outputs and in the right there is the weight matrix.

In the weight matrix, red means negative and blue positive, and the intensity of color is roughly the 
logarithm of the absolute value of the weight. It can be rearranged in 3 ways:

- raw: the adjacency matrix as it is. Each row is the source neuron and each column has the value of influence in the target neuron.
- pre-synaptic: arranged in rectangles, each one is a neuron, with each subrectangle being the weight 
with the next neuron. This view shows how one neuron affects to the others. 
In other words, before the synapse with the next neuron.
- post-synaptic: arranged in rectangles, each one is a neuron, with each subrectangle being the weight 
with the previous neuron. This view shows how one neuron is affected by the others. This is the one showing in the above image.

For example, in the center of the weight matrix you can see a couple of red '4', surrounded by some blue '4'. The red '4'
neurons will turn off or on if the global pattern seems like a '4' or not, respectively. They will behave exactly the opposite 
 way as the blue '4' ones.

#### Net description

This specific instance is using fixed thresholds, synchronous activation, binary pulses and the 
hebbian rule for learning.

Unfortunately, as it is a very simple configuration, 
those patterns have similar structure and the net is one layer deep, it achieves to learn only 3 patterns but 
struggles to learn 4 or 5. 

On the other side, as it is such a simple net, it only needs a few iterations (around 50 input patterns).
As it was expected, it is able to reconstruct some errors in the learnt patterns, which is the core idea 
of the autoassociative memory: retrieve a whole pattern using part of it.


