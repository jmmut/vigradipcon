#ifndef _FRECUENCIADOR_H_
#define  _FRECUENCIADOR_H_
#include <iostream>
#include <string>
using namespace std;

/**
* Calcula si una señal tiene una frecuencia superior o inferior
* a la indicada (en forma de periodo)
*/
class Frecuenciador {
private:
    short int tti;	// time to ignore last signal
public:
    short int periodo;
    int lastDeviation;
    Frecuenciador();

    int Potenciar();
    void Potenciar(int &valor);
    void Potenciar(short int &valor);
    void Tick();
    void Reset();
    int FrecuenciaSuperada();
    int Desviacion();
    //void Potenciar();
    //int get ();
    // deprimir
    void setPeriodo(int periodo);
    void updatePeriodo(int diferencia);
    string toString();

};


#endif /*_FRECUENCIADOR_H_*/
