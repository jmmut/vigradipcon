
#include "Gestor.h"
#include "Memoria.h"

using namespace randomize;

/**
* Reescala el valor en un marco logaritmico.
* En otras palabras devuelve lo que se veria en un plot con ejes
* pseudologaritmicos: 2^2, 2^1, 2^0, 0, -2^0, -2^1, -2^2, etc.
*/
int Gestor1::Lg (int x)
{
    int res(0);
    bool negativo(x<0);

    if (x == 0)
        return 0;

    if (negativo)
        x *= -1;

    while (x > 0)
    {
        res ++;
        x >>= 1;
    }

    if (negativo)
        res *= -1;

    return res;
}

/**
* Calcula el color logaritmico correspondiente.
* *1: no puede haber valor mayor que 2^32, y los distribuimos porque
* queremos que los colores se distingan lo maximo posible. 8*32=256.
* @param degradado: si no se pide degradado sera siempre verde
*/
Color Gestor1::ColorLg2 (int x, bool degradado)
{
    int peso = Lg(x)*8;	// *1
    if (!degradado)
        return *Color().set(0, 255, 0, 255);	//verde : no hay conexion
    else
    if (peso >= 0)
        return *Color().set(0, 0, peso, 255);	// azul : mayor que 0
    else
        return *Color().set(-peso, 0, 0, 255);	// rojo : menor que 0
}

Gestor1::Gestor1() : Ventana(1250, 680, SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL) {
    int i;

    n = 35;
    nx = 7;
    ny = 5;
    frame = 10;
    pcx = 16;
    pcy = 32;
    pw = 15;
    pwx = 9;
    pwy = 18;
    //n_hist = 4;
    n_entr = 5;
    g = new Gradipcon(n, n, n);
    reset();
    g->setModo(Gradipcon::hebbiano);
    setFps(20);

    entr = new bool*[n_entr];
    for(i = 0; i < n_entr; i++)
        entr[i] = new bool[g->ne];

    g->abrirEntrada ("jmmut/vigradipcon/entrenamiento35.txt", f);
    g->cargarEntrada (f, entr[3], g->ne);
    g->cargarEntrada (f, entr[4], g->ne);
    f.close();

//    for (i = 0; i < g->ne; i++)
//        entr[4][i] = 0;

    g->abrirEntrada ("jmmut/vigradipcon/ruido.txt", f);
    g->cargarEntrada (f, entr[0], g->ne);
    g->cargarEntrada (f, entr[1], g->ne);
    g->cargarEntrada (f, entr[2], g->ne);
    f.close();

    g->abrirEntrada ("jmmut/vigradipcon/ruido2.txt", f);
    g->cargarEntrada (f, entr[0], g->ne);
    g->cargarEntrada (f, entr[1], g->ne);
    g->cargarEntrada (f, entr[2], g->ne);
    f.close();

    entr_actual = 0;
    entr_auto_actual = 0;
    entr_auto = true;
    testBox = false;
    iter = 0;
    lock_learning = false;

    color_on.set(32, 32, 192, 255);
    color_off.set(32, 32, 32, 255);
    color_bg.set(16, 16, 16, 255);
    modo_pesos = Distribution::RAW;

    layout.miscellaneous.cursor = nullptr;

    initGL();
    int width;
    int height;
    SDL_GetWindowSize(window, &width, &height);

    initUi(width, height);
    createButtons(width, height);
    moveButtons(modo_pesos);

    semaforoStep.cerrar();
    semaforoDisplay.cerrar();
    semaforoDisplay.sumar();

}

Gestor1::~Gestor1() {
    if (layout.miscellaneous.cursor != nullptr) {
        delete layout.miscellaneous.cursor;
    }
    if (layout.miscellaneous.autoInput != nullptr) {
        delete layout.miscellaneous.autoInput;
    }
    if (layout.miscellaneous.autoDrawing != nullptr) {
        delete layout.miscellaneous.autoDrawing;
    }
    if (layout.miscellaneous.autoComputing != nullptr) {
        delete layout.miscellaneous.autoComputing;
    }
    for (auto &gradient : layout.miscellaneous.gradient) {
        delete gradient;
    }
    for (auto &item : layout.entradas) {
        delete item;
    }
    for (auto &item : layout.salidas) {
        delete item;
    }
    for (std::pair<const std::tuple<int, int>, UI::Button*> &item : layout.pesos) {
        delete item.second;
    }
    for (auto &item : layout.neuronThresholds) {
        delete item;
    }
    for (auto &item : layout.neuronInputWeights) {
        delete item;
    }
}


void Gestor1::initGL() {

    GLdouble aspect;
    int width = 640, height = 480;

    LOG_INFO("Creating Ventana with custom initGL\n");
    if (window == NULL) {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                "There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
    } else {
        SDL_GetWindowSize(window, &width, &height);
        if (context == NULL) {   //Don't create a new context if it already exists;
            context = SDL_GL_CreateContext(window);
        }
        if (!context) {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to create OpenGL context: %s\n", SDL_GetError());
            SDL_Quit();
            exit(2);
        }
    }

    glViewport(0, 0, width, height);
    glClearColor(color_bg.getRf(), color_bg.getGf(), color_bg.getBf(), color_bg.getAf());        // This Will Clear The Background Color To Black
    glClearDepth(1.0);                // Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS);                // The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST);            // Enables Depth Testing
    glEnable(GL_TEXTURE_2D);            // Enables Depth Testing
    glShadeModel(GL_SMOOTH);            // Enables Smooth Color Shading

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();                // Reset The Projection Matrix

    aspect = (GLdouble) width / height;

//    perspectiveGL(45, aspect, 0.1, 100);

    glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
}

void Gestor1::onStep(float dt)
{
    g->Acumular (entr[entr_actual]);
    g->Activar(*hist.getNuevo());
    //if (!Iguales (ent, sal, ns) && iter < itermax)
    if (!lock_learning) {
        g->Reconfigurar (entr[entr_actual], hist.get());
    }
    iter++;
    entr_auto_actual = Modulo(entr_auto_actual+1, 3);
    if (entr_auto) {
        entr_actual = entr_auto_actual;
    }
    moveMiscellaneousButtons();
}

void Gestor1::onDisplay()
{
    // Clear The Screen And The Depth Buffer
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

//    LOG_DEBUG("ondisplay");
    panel.Draw();

    SDL_GL_SwapWindow(window);
}

int sp(8);	// espacio entre cuadros

void Gestor1::onResize(int width, int height) {
    initGL();
    initUi(width, height);
    moveButtons(modo_pesos);
    semaforoDisplay.sumar();
}

void Gestor1::onMouseButton(SDL_Event &e) {
    if (e.button.type == SDL_MOUSEBUTTONDOWN) {
        panel.CB_Click(UI::Event::UI_EVENT_DOWN, e.button.x, e.button.y);
    } else if (e.button.type == SDL_MOUSEBUTTONUP) {
        panel.CB_Click(UI::Event::UI_EVENT_UP, e.button.x, e.button.y);
    }
    semaforoDisplay.sumar();
}

void Gestor1::onKeyPressed(SDL_Event &e)
{
    if (e.type == SDL_KEYDOWN)
        return;


    switch(e.key.keysym.sym)
    {
        case SDLK_SPACE:
            semaforoDisplay.sumar();
            semaforoStep.sumar();
            break;
        case SDLK_p:	// play / stop
            layout.miscellaneous.autoDrawing->CB_Click(UI::UI_EVENT_UP, 0, 0);
            layout.miscellaneous.autoComputing->CB_Click(UI::UI_EVENT_UP, 0, 0);
            break;
        case SDLK_PLUS:
        case SDLK_KP_PLUS:
            g->forEachNeurona([](Neurona &neurona, int index){
                neurona.umbral++;
            });
            //pcx *= 2;
            //pcy *= 2;
            break;
        case SDLK_MINUS:
        case SDLK_KP_MINUS:
            g->forEachNeurona([](Neurona &neurona, int index){
                neurona.umbral--;
            });
            //pcx *= 0.5;
            //pcy *= 0.5;
            break;
        case SDLK_r:	// redraw xD
            break;
        case SDLK_a:	// vision en matriz
            modo_pesos = Distribution::RAW;
            snprintf(title, 100, "raw view (for other views, type 'a', 's' or 'd')");
            SDL_SetWindowTitle(window, title);
            moveButtons(modo_pesos);
            break;
        case SDLK_s:	// vision postsinaptica
            modo_pesos = Distribution::POSTSYNAPTIC;
            snprintf(title, 100, "post-synaptic view (for other views, type 'a', 's' or 'd')");
            SDL_SetWindowTitle(window, title);
            moveButtons(modo_pesos);
            break;
        case SDLK_d:	// vision presinaptica
            modo_pesos = Distribution::PRESYNAPTIC;
            snprintf(title, 100, "pre-synaptic view (for other views, type 'a', 's' or 'd')");
            SDL_SetWindowTitle(window, title);
            moveButtons(modo_pesos);
            break;
        case SDLK_w:
            g->verPesos();
            break;
        case SDLK_u:
            g->verUmbrales();
            break;
        case SDLK_e:
            layout.miscellaneous.autoInput->CB_Click(UI::UI_EVENT_UP, 0, 0);
            break;
        case SDLK_0:
        case SDLK_KP_0:
            entr_actual = 0;
            moveMiscellaneousButtons();
            break;
        case SDLK_1:
        case SDLK_KP_1:
            entr_actual = 1;
            moveMiscellaneousButtons();
            break;
        case SDLK_2:
        case SDLK_KP_2:
            entr_actual = 2;
            moveMiscellaneousButtons();
            break;
        case SDLK_3:
        case SDLK_KP_3:
            entr_actual = 3;
            moveMiscellaneousButtons();
            break;
        case SDLK_4:
        case SDLK_KP_4:
            entr_actual = 4;
            moveMiscellaneousButtons();
            break;
        case SDLK_h:
            snprintf(title, 100, "help written in the console");
            SDL_SetWindowTitle(window, title);
            MostrarAyuda();
            break;
        case SDLK_z:
            g->randomize(5, 0);
            break;
        case SDLK_x:
            reset();
            break;
        case SDLK_m: {
            int modo = 0;
            SDLcin(modo);
            g->setModo(Gradipcon::Modo(modo));
            SDL_SetWindowTitle(window, (string("modo: ") + to_string(modo)).c_str());
            break;
        };
        case SDLK_t:
            testBox = !testBox;
            UI::setTestBox(testBox);
            break;
        case SDLK_f: {
            snprintf(title, 100, "current frames per second: %d, new value:", getFps());
            int fps;
            SDL_SetWindowTitle(window, title);
            SDLcin(fps);
            setFps(fps);
            snprintf(title, 100, "frames per second set to: %d", getFps());
            SDL_SetWindowTitle(window, title);
            break;
        };
        case SDLK_l:
            lock_learning = !lock_learning;
            break;
        default:
            break;
    }
    semaforoDisplay.sumar();
}

void Gestor1::MostrarAyuda()
{
    cout << "Accion de las teclas: \n\n";

    cout << "SPACE: avanzar un paso" << endl;
    cout << "p: play/stop, avanzar indefinidamente" << endl;
    cout << "r: redraw" << endl;
    cout << "a: vision en matriz" << endl;
    cout << "s: vision postsinaptica, cada cuadro engloba sus entradas" << endl;
    cout << "d: vision presinaptica, cada cuadro engloba a las que afecta" << endl;
    cout << "w: ver pesos" << endl;
    cout << "u: ver umbrales" << endl;
    cout << "e: entrada automatica" << endl;
    cout << "0-4: elegir siguiente entrada" << endl;
    cout << "h: mostrar esta ayuda" << endl;
    cout << "z: randomize" << endl;
    cout << "x: reset" << endl;
    cout << "l: lock learning" << endl;
    cout << "m: change neural net mode" << endl;
    cout << "t: enable test boxes" << endl;
    cout << "f: change frames per second" << endl;
    cout << endl;
}

int Gestor1::getGradientTexture(int peso) {
    int colorIdx = Lg(peso);
    if (colorIdx >= 0) {
        return 31 - colorIdx;
    } else {
        return colorIdx + 63;
    }
}

int Gestor1::getNeuronTexture(bool onOrOff) {
    return onOrOff;
}

void Gestor1::putPixel(vector<unsigned char> &texture, Color color) {
    texture.push_back(color.r);
    texture.push_back(color.g);
    texture.push_back(color.b);
    texture.push_back(color.a);
}

/**
 * creates the textures and the buttons.
 * Does not set the color nor the position of the buttons.
 */
void Gestor1::createButtons(int width, int height) {
    // ------------------- create textures
    vector<unsigned char> degradado;

    for(int color = 0x40000000, yi = 0; yi < 32; color >>= 1, yi++) {// vector de muestra de color
        Color colorIdx(ColorLg2(color, true));
        putPixel(degradado, colorIdx);
//            degradado.push_back(0x7F);
    }
    for(int color = 0x40000000, yi = 0; yi < 32; color >>= 1, yi++) {// vector de muestra de color
        Color colorIdx(ColorLg2(-color, true));
        putPixel(degradado, colorIdx);
//            degradado.push_back(0x7F);
    }
    for (int j = 0; j < 8; ++j) {
        LOG_EXP(degradado[j], "%d")
        LOG_EXP(degradado[j + degradado.size()/2], "%d")
    }

    vector<unsigned char> neuronTextures;
    putPixel(neuronTextures, color_off);
    putPixel(neuronTextures, color_on);

    int imgGradient = LoadImg(degradado, degradado.size() / 4, 1);
    int imgNeuron = LoadImg(neuronTextures, neuronTextures.size() / 4, 1);
    LOG_EXP(degradado.size(), "%d")

    // --------------- create buttons
    UI::Button::Conf gradientConf = {(unsigned int) imgGradient, (int) degradado.size() / 4, 1, 0, 0, 0, 0, pw - 5, pw - 5};

    // weights
    unsigned int id = 0;
    g->forEachArco([&](Arco &arco, const int &from, const int &to) {
        SelfDrawableButton *boton = new SelfDrawableButton();
        boton->setConfig(gradientConf);
        boton->setListener([&, from, to](UI::Event action, int x, int y, UI::Element *this_element, void *user_data) {
            LOG_DEBUG(" en boton from: %d, to: %d", from, to);
            LOG_DEBUG(" en boton x: %d, y: %d", x, y);
            LOG_DEBUG("this_element->getId() = %d", this_element->getId());
            LOG_EXP(g->arcos[from][to].peso, "%d");
            snprintf(title, 100, "arco desde el nodo %d al %d, peso: %d", from, to, g->arcos[from][to].peso);
            SDL_SetWindowTitle(window, title);
        }, nullptr);
        boton->setTextureChooser([&arco, this](SelfDrawableButton *me) {
            int weight = getGradientTexture(arco.peso);
            me->setButtonFrames(weight, 0, weight, 0);
        });
        boton->setId(id);
        panel.addElemento(boton, id++);
        layout.pesos[tuple<int, int>(from, to)] = boton;
    });

    // neurons
    g->forEachNeurona([&](Neurona &neurona, const int &index) {
        SelfDrawableButton *boton = new SelfDrawableButton();
        boton->setConfig(gradientConf);
        boton->setListener([this, index, &neurona](UI::Event action, int x, int y, UI::Element *this_element, void *user_data) {
            snprintf(title, 100, "neuron %d, threshold %d, accumulated stimulus %d, input weight %d, period %d, lastDeviation %d",
                    index,
                    neurona.umbral,
                    neurona.last_estimulo,
                    g->arcos[g->max][index].peso,
                    neurona.f.periodo,
                    neurona.f.lastDeviation);
            SDL_SetWindowTitle(window, title);
        }, nullptr);
        boton->setTextureChooser([&neurona, this](SelfDrawableButton *me) {
            int threshold = getGradientTexture(neurona.umbral);
            me->setButtonFrames(threshold, 0, threshold, 0);
        });
        boton->setId(id);
        panel.addElemento(boton, id++);
        layout.neuronThresholds.push_back(boton);
    });

    // input weights
    g->forEachArcoEntrada([&](Arco &arco, const int &to){
        SelfDrawableButton *boton = new SelfDrawableButton();
        boton->setConfig(gradientConf);
        boton->setListener([this, &arco, to](UI::Event action, int x, int y, UI::Element *this_element, void *user_data) {
            snprintf(title, 100, "neuron %d, input weight %d", to, arco.peso);
            SDL_SetWindowTitle(window, title);
        }, nullptr);
        boton->setTextureChooser([&arco, this](SelfDrawableButton *me) {
            int weight = getGradientTexture(arco.peso);
            me->setButtonFrames(weight, 0, weight, 0);
        });
        boton->setId(id);
        panel.addElemento(boton, id++);
        layout.neuronInputWeights.push_back(boton);
    });

    LOG_DEBUG("before gradients, the id is %d", id);
    // gradient
    for (int l = 0; l < 64; ++l) {
        SelfDrawableButton *boton = new SelfDrawableButton();
        boton->setConfig(gradientConf);
        boton->setListener([=](UI::Event action, int x, int y, UI::Element *this_element, void *user_data){
            LOG_DEBUG("on gradient button %d, id: %d", l, id);
        }, nullptr);
        boton->setTextureChooser([l](SelfDrawableButton *me) {
            me->setButtonFrames(l, 0, l, 0);
        });
        panel.addElemento(boton, id++);
        layout.miscellaneous.gradient.push_back(boton);
    }

    // outputs
    UI::Button::Conf neuronButton((unsigned int) imgNeuron, (int) neuronTextures.size() / 4, 1, 0, 0, 0, 0, pcx -1, pcy - 1);
    int groupIndex = 0;
    hist.goFakeHead();
    for (int m = 0; m < hist.size(); ++m) {
        bool *value = hist.get();
        for (int i = 0; i < nx * ny; ++i) {
            SelfDrawableButton *boton = new SelfDrawableButton();
            boton->setConfig(neuronButton);
            boton->setListener([=](UI::Event action, int x, int y, UI::Element *this_element, void *user_data) {
                LOG_DEBUG("output group %d, x: %d, y:%d", groupIndex, i%nx, i/nx);
            }, nullptr);
            boton->setTextureChooser([this, m, i](SelfDrawableButton *me) {
                me->setButtonFrames(this->hist[m][i], 0, this->hist[m][i], 0);
            });
            panel.addElemento(boton, id++);
            layout.salidas.push_back(boton);
        }
        groupIndex++;
    }

    // inputs
    UI::Button::Conf switchButton((unsigned int) imgNeuron, (int) neuronTextures.size() / 4, 1, 0, 0, 1, 0, pcx -1, pcy - 1);
    for (int k = 0; k < n_entr; ++k) {
        for (int i = 0; i < nx * ny; ++i) {
            UI::Switch *boton = new UI::Switch();
            boton->setConfig(switchButton);
            boton->setListener([=](UI::Event action, int x, int y, UI::Element *this_element, void *user_data) {
                if (action == UI::Event::UI_EVENT_UP) {
                    entr[k][i] = !entr[k][i];
                    LOG_DEBUG("input group %d, x: %d, y:%d, value:%d", k, i%nx, i/nx, entr[k][i]);
                }
            }, nullptr);
            boton->setStatus(entr[k][i]);
            panel.addElemento(boton, id++);
            layout.entradas.push_back(boton);
        }
    }


    // Miscellaneous
    UI::Button *boton = new UI::Button();
    boton->setConfig(neuronButton);
    panel.addElemento(boton, id++);
    layout.miscellaneous.cursor = boton;

    UI::Switch *swButton = new UI::Switch();
    swButton->setConfig(switchButton);
    swButton->setListener([&](UI::Event action, int x, int y, UI::Element *this_element, void *user_data) {
        if (action == UI::Event::UI_EVENT_UP) {
            semaforoDisplay.toggle();
        }
    }, nullptr);
    panel.addElemento(swButton, id++);
    layout.miscellaneous.autoDrawing = swButton;

    swButton = new UI::Switch();
    swButton->setConfig(switchButton);
    swButton->setListener([&](UI::Event action, int x, int y, UI::Element *this_element, void *user_data) {
        if (action == UI::Event::UI_EVENT_UP) {
            semaforoStep.toggle();
        }
    }, nullptr);
    panel.addElemento(swButton, id++);
    layout.miscellaneous.autoComputing = swButton;

    swButton = new UI::Switch();
    swButton->setConfig(switchButton);
    swButton->setListener([&](UI::Event action, int x, int y, UI::Element *this_element, void *user_data) {
        if (action == UI::Event::UI_EVENT_UP) {
            entr_auto = !entr_auto;
        }
    }, nullptr);
    swButton->setStatus(true);
    panel.addElemento(swButton, id++);
    layout.miscellaneous.autoInput = swButton;
}

void Gestor1::initUi(int width, int height) {
    panel.setLayout(UI::Layout::LAYOUT_CENTER);
//    panel.setPos(2, 2);
    UI::setTestBox(testBox);
    UI::setConfWindow(width, height);
}


void Gestor1::moveButtons(Distribution distribution) {

    int x0 = hist.size() * (nx + 1) * pcx + frame;
    int y0 = frame;
    int displays = 3;
    int width;
    int height;
    SDL_GetWindowSize(window, &width, &height);

    // weights
    switch (distribution) {
        case Distribution::RAW:
            x0 += pw/2;
            y0 += pw/2;
            g->forEachArco([&](Arco &arco, const int &from, const int &to) {
                UI::Button * boton = layout.pesos[tuple<int,int>(from,to)];
                boton->setPosPixel(to*pw+x0, from*pw+y0);
                boton->setTamPixel(pw-1, pw-1);
                if (displays-- > 0) {
                    LOG_DEBUG("setting button at %d, %d", to*pw+x0, from*pw+y0)
                    LOG_DEBUG(" en boton from: %d, to: %d ", from, to);
                }
            });
            break;
        case Distribution::POSTSYNAPTIC:
            x0 += pwx/2;
            y0 += pwy/2;
            g->forEachArco([&](Arco &arco, const int &from, const int &to) {
                UI::Button * boton = layout.pesos[tuple<int,int>(from,to)];
                moveButton(to*n + from, x0, y0, pwx, pwy, nx, ny, nx, ny, [&](int xPos, int yPos){
                    boton->setPosPixel(xPos, yPos);
                });
                boton->setTamPixel(pwx-1, pwy-1);
            });
            break;
        case Distribution::PRESYNAPTIC:
            x0 += pwx/2;
            y0 += pwy/2;
            g->forEachArco([&](Arco &arco, const int &from, const int &to) {
                UI::Button * boton = layout.pesos[tuple<int,int>(from,to)];
                moveButton(from*n + to, x0, y0, pwx, pwy, nx, ny, nx, ny, [&](int xPos, int yPos){
                    boton->setPosPixel(xPos, yPos);
                });
                boton->setTamPixel(pwx-1, pwy-1);
            });
            break;
    }

    // thresholds
    x0 = hist.size() * (nx + 1) * pcx + frame; // mid bottom
    y0 = height - frame;
    switch (distribution) {
        case Distribution::RAW:
            x0 += pw/2;
            y0 += -pw + pw/2;
            moveButtonGroup(layout.neuronThresholds, x0, y0, pw, pw, n, 1);
            break;
        case Distribution::POSTSYNAPTIC:
        case Distribution::PRESYNAPTIC:
            x0 += pwx/2;
            y0 += - ny * pwy + pwy/2;
            moveButtonGroup(layout.neuronThresholds, x0, y0, pwx, pwy, nx, ny);
            break;
    }

    // input weights
    x0 = hist.size() * (nx + 1) * pcx + frame;
    y0 = height/2;
    switch (distribution) {
        case Distribution::RAW:
            x0 += -(layout.neuronInputWeights.size() + 1)*pw + pw/2;
//            y0 += -pw + pw/2;
            moveButtonGroup(layout.neuronInputWeights, x0, y0, pw, pw, n, 1);
            break;
        case Distribution::POSTSYNAPTIC:
        case Distribution::PRESYNAPTIC:
            x0 += - nx*pwx - pwx + pwx/2;
            y0 += - ny * pwy / 2 + pwy/2;
            moveButtonGroup(layout.neuronInputWeights, x0, y0, pwx, pwy, nx, ny);
            break;
    }

    //gradient
    LOG_EXP(layout.miscellaneous.gradient.size(), "%d");
    x0 = hist.size() * (nx + 1) * pcx + frame + (n*pw) + pw;   // top right corner
    y0 = frame + pw/2;
    displays = 3;
    moveButtonGroup(layout.miscellaneous.gradient, x0, y0, pw, pw, 1, layout.miscellaneous.gradient.size()/2, 2, 1);

    // outputs
    x0 = frame + pcx/2;    // top left corner: outputs
    y0 = frame + pcy/2;
    moveButtonGroup(layout.salidas, x0, y0, pcx, pcy, nx, ny);

    x0 = frame + pcx/2;    // bottom left corner: inputs
    y0 = -frame + height - ny * pcy + pcy/2;
    moveButtonGroup(layout.entradas, x0, y0, pcx, pcy, nx, ny);

    // miscellaneous
    moveMiscellaneousButtons();
}

void Gestor1::moveMiscellaneousButtons() {
    int width;
    int height;
    SDL_GetWindowSize(window, &width, &height);
    int x0 = frame + pcx/2;    // bottom left corner: inputs
    int y0 = -frame + height - ny * pcy + pcy/2;
    layout.miscellaneous.cursor->setPosPixel(x0 + pcx /2 + pcx * (nx +1) * entr_actual, y0 - pcx - pcy /2);
    layout.miscellaneous.cursor->setTamPixel(pcy-1, pcx-1);
    x0 = frame + pcy /2;
    y0 = height / 2;
    layout.miscellaneous.autoDrawing->setPosPixel(x0 , y0);
    layout.miscellaneous.autoDrawing->setTamPixel(pcy-1, pcx-1);
    layout.miscellaneous.autoComputing->setPosPixel(x0, y0 + pcx);
    layout.miscellaneous.autoComputing->setTamPixel(pcy-1, pcx-1);
    layout.miscellaneous.autoInput->setPosPixel(x0, y0 + pcx + pcx);
    layout.miscellaneous.autoInput->setTamPixel(pcy-1, pcx-1);
}

void Gestor1::moveButtonGroup(vector<UI::Button *> &buttons, int x0, int y0, int buttonWidth, int buttonHeight,
        int columns, int rows, int superColumns, int superRows, int startIndex, int endIndex) {
    int squares = endIndex == -1 ? (int) buttons.size() : endIndex;
    superColumns = superColumns == -1 ? (int) buttons.size() : superColumns;
    superRows = superRows == -1 ? (int) buttons.size() : superRows;
    for (int i = startIndex; i < squares; ++i) {
        moveButton(i, x0, y0, buttonWidth, buttonHeight, columns, rows, superColumns, superRows,
                [=, &buttons](int xPos, int yPos) {
                    buttons[i]->setPosPixel(xPos, yPos);
                    buttons[i]->setTamPixel(buttonWidth - 1, buttonHeight - 1);
                }
        );
    }
}

void Gestor1::moveButton(int index, int x0, int y0, int buttonWidth, int buttonHeight, int columns, int rows,
        int superColumns, int superRows, std::function<void(int xPos, int yPos)> callback) {
    int groupWidth = columns * buttonWidth + buttonWidth;
    int groupHeight = rows * buttonHeight + buttonHeight;
    int groupIndex = index /(columns * rows);
    int superGroupX = (groupIndex % superColumns) * groupWidth;
    int superGroupY = ((groupIndex / superColumns) % superRows)* groupHeight;
    int columnIndex = index % columns;
    int rowIndex = (index / columns) % rows;   // taking into account that all the groups start at the same height
    callback(x0 + superGroupX + columnIndex * buttonWidth, y0 + superGroupY + rowIndex * buttonHeight);
}




//void Gestor1::paintButtons() {
    // weights
//    g->forEachArco([&](Arco &arco, const int &from, const int &to) {
//        UI::Button * boton = layout.pesos[tuple<int,int>(from,to)];
//        boton->setButtonFrames(getGradientTexture(arco.peso), 0, getGradientTexture(arco.peso), 0);
//    });

    //gradient
//    for (int k = 0; k < 64; ++k) {
//        layout.miscellaneous.gradient[k]->setButtonFrames(k, 0, k, 0);
//    }

    // outputs
//    int groupIndex = 0;
//    hist.goFakeHead();
//    while (hist.goNext()) {
//        bool *value = *(hist.get());
//        for (int i = 0; i < nx * ny; ++i) {
//            UI::Button *button = layout.salidas[i + groupIndex * nx * ny];
//            button->setButtonFrames(getNeuronTexture(value[i]), 0, getNeuronTexture(value[i]), 0);
//        }
//        groupIndex++;
//    }

    // inputs
//    groupIndex = 0;
//    for (int j = 0; j < n_entr; ++j) {
//        bool *value = entr[j];
//        for (int i = 0; i < nx * ny; ++i) {
//            UI::Button *button = layout.entradas[i + groupIndex * nx * ny];
//            button->setButtonFrames(getNeuronTexture(value[i]), 0, getNeuronTexture(value[i]), 0);
//        }
//        groupIndex++;
//    }

    // miscellaneous
//    paintMiscellaneousButtons();
//}

//void Gestor1::paintMiscellaneousButtons() {
//    layout.miscellaneous.autoInput->setButtonFrames(getNeuronTexture(entr_auto), 0, getNeuronTexture(entr_auto), 0);
//    layout.miscellaneous.autoDrawing->setButtonFrames(getNeuronTexture(semaforoDisplay.estado()), 0, getNeuronTexture(
//            semaforoDisplay.estado()), 0);
//    layout.miscellaneous.autoComputing->setButtonFrames(getNeuronTexture(semaforoStep.estado()), 0, getNeuronTexture(
//            semaforoStep.estado()), 0);
//}
void Gestor1::reset() {
    g->reset(0, 0);
}
