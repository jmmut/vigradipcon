#ifndef _GESTOR_H_
#define _GESTOR_H_

#include "vgm/Ventana.h"
#include "UI/UI.h"
#include "UI/Boton.h"
#include "UI/Panel.h"
#include "UI/Element.h"
#include "UI/Switch.h"
#include "SelfDrawableButton.h"

#include "Memoria.h"

class Gestor1: public Ventana, public Memoria
{
public:
    Gestor1();
    ~Gestor1();
    void initGL();
    void onStep(float) override;
    void onDisplay() override;
    void onKeyPressed(SDL_Event &e) override;
    void onResize(int width, int height) override;
    void onMouseButton (SDL_Event &e) override;

    void createButtons(int width, int height);
    void initUi(int width, int height);
    void moveButtons(Distribution distribution);
    void MostrarAyuda();
private:
    int Lg (int x);
    Color ColorLg2 (int x, bool degradado);
    void putPixel(vector<unsigned char> &texture, Color color);
    int getGradientTexture(int peso);
    int getNeuronTexture(bool onOrOff);
    void moveButtonGroup(vector<randomize::UI::Button *> &buttons, int x0, int y0, int buttonWidth, int buttonHeight,
            int columns, int rows, int superColumns = -1, int superRows = -1, int startIndex = 0, int endIndex = -1);
    void moveButton(int index, int x0, int y0, int buttonWidth, int buttonHeight, int columns, int rows, int superColumns,
            int superRows, function<void(int xPos, int yPos)> callback);
    void moveMiscellaneousButtons();

    SDL_Surface *screen;
    SDL_Renderer *renderer;
    SDL_Texture *texture;

    char *title = new char[100];

    randomize::UI::Panel<35*35*2> panel;
    bool testBox;
    bool lock_learning;

    void reset();
};

#endif
