#ifndef _MEMORIA_H_
#define _MEMORIA_H_

#include <fstream>
#include <SDL.h>
#include <randomize/common-libs/src/UI/Boton.h>
#include "vgm/Ventana.h"
#include "gradipcon.h"

enum class Distribution {
    RAW,
    POSTSYNAPTIC,
    PRESYNAPTIC
};

class Color {
public:
    union {
        Uint32 conjunto;
        struct
        {
            Uint8 r;
            Uint8 g;
            Uint8 b;
            Uint8 a;
        };
        Uint8 c[4];
    };
    Color() : Color(0) {}
    Color (int color) {
        conjunto = color;
    }
    Color* set(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
        this->r = r;
        this->g = g;
        this->b = b;
        this->a = a;
        return this;
    }
    float getRf() {return r/255.0;}
    float getGf() {return g/255.0;}
    float getBf() {return b/255.0;}
    float getAf() {return a/255.0;}
    float getCf(int index) {return c[index]/255.0;}
};

struct Layout {
    vector<randomize::UI::Button *> entradas;
    vector<randomize::UI::Button *> salidas;
    std::map<tuple<int, int>, randomize::UI::Button*> pesos;
    vector<randomize::UI::Button *> neuronThresholds;
    vector<randomize::UI::Button *> neuronInputWeights;
    struct {
        randomize::UI::Button * cursor;
        randomize::UI::Button * autoInput;
        randomize::UI::Button * autoDrawing;
        randomize::UI::Button * autoComputing;
        vector<randomize::UI::Button *> gradient;
    } miscellaneous;
};

int Modulo(int n, int m);
int LoadPNG(const char *filename);
int LoadImg(std::vector<unsigned char> image, unsigned int width, unsigned int height);
#include "Lista.hpp"
struct Memoria
{
    int n;	// numero de neuronas del gradipcon
    //int n_hist;	// profundidad del historico de salidas
    int n_entr;	// cantidad de entradas disponibles
    int frame;
    int nx;	// si las neuronas estan organizadas en una matriz
    int ny;
    int pcx;	// pixeles por cuadro en x
    int pcy;
    int pw, pwx, pwy;	//si lo quieres cuadrado, o no
    bool **entr;	//entradas disponibles
    struct bool35{bool v[35];};
    Lista<bool[35], 5> hist;
//    vector<bool[35]> hist;
    //bool **hist;	// historico de salidas
    Gradipcon *g;
    ifstream f;
    //int sum1), pasadas(0);
    int entr_actual;
    int entr_auto_actual;
    int entr_auto;
    //int hist_actual;
    int iter;
    Color color_on, color_off, color_bg;
    Layout layout;
    Distribution modo_pesos;
};

#endif
