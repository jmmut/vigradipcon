//
// Created by jmmut on 2015-06-29.
//

#ifndef VIGRADIPCON_SELFDRAWABLEBUTTON_H
#define VIGRADIPCON_SELFDRAWABLEBUTTON_H

#include "randomize/common-libs/src/UI/Button.h"
#include <functional>

class SelfDrawableButton : public randomize::UI::Button {
public:
    void setTextureChooser(std::function<void(SelfDrawableButton *)> chooseTexture) {
        textureChooser = chooseTexture;
    }
    void Draw() {
        textureChooser(this);
        randomize::UI::Button::Draw();
    }
private:
    std::function<void (SelfDrawableButton *)> textureChooser;
};


#endif //VIGRADIPCON_SELFDRAWABLEBUTTON_H
