/***********************************************************
* 
* 		autor: 	Jose Miguel Mut
* 
*		fecha:	2012-06-14
*
* 		archivo:	grafo.cpp
* 
* 		proyecto:	grafo neuronal
* 
* 		descripcion:
* 
**********************************************************/

#include <randomize/common-libs/src/log/log.h>
#include "gradipcon.h"


#define DEPURAR
/**
 * Construye el grafo, con las variables opcionales:
 * num: neuronas activas. si se omite hay 0
 * maximo: neuronas usables. si se omite sera MAX_NEURONAS
 * ent: maximo neuronas con entrada sensorial: si se omite sera MAX_NEURONAS
 * sal: maximo neuronas cuyo estado sale del sistema: si se omite sera MAX_NEURONAS
 */
Gradipcon::Gradipcon(int num, int ent, int sal, int maximo):
        n(num), max(maximo), maxe(ent), maxs(sal)
{
    int i, j;

    if (n < maxe)	// error, n debe ser mayor
        n = maxe;

    if (n < maxs)	// error
        n = maxs;

    if (max < n)	// error, max debe ser mayor a todos
        max = n;

    neu = new Neurona*[max];	// lista de neuronas : max punteros
    for(i = 0; i < n; i++)	// se crean n objetos, no max
        neu[i] = new Neurona;

    for (i = n; i < max; i++)	// de n a max son NULL
        neu[i] = NULL;

    arcos = new Arco*[max + 1];	// matriz de adyacencia + pesos entradas
    for (i = 0; i < n; i++)	// en la primera parte de la matriz n*max
    {
        arcos[i] = new Arco[max];	// aunque no lo usemos de momento vale la pena no realocar

        for (j = n; j < max; j++)
            arcos[i][j].existe = false;	// parte derecha de la matriz de pesos
    }

    for (i = n; i < max; i++)
        arcos[i] = NULL;

    arcos[max] = new Arco[max];	// fila extra

    for (i = 0; i < max; i++)
    {
        //arc[i][i].existe = false;	// excepto consigo misma (diagonal)
        //arc[i][i].p = 0;	// excepto consigo misma (diagonal)
        arcos[max][i].existe = false;	// y como entrada (ultima fila)
        //arc[max][i].p = 0;	// cuando se ponga una conexion a true, debe ponerse el peso a 0
    }

    entradas = new int [maxe];	// vectores de e/s (maximos)
    salidas = new int [maxs];

    ne = maxe;		// numero existente (usados) de e/s
    ns = maxs;
    setEntradas (ne);
    setSalidas (ns);

    modo = 3;
    last_ent = new bool[ne];
//	neu[0]->f.setPeriodo(3);
/*
    i = 0;
    for (string nombre : modos) {
        nombres_modos.insert({nombre, i});
        i++;
    }
*/

    reset(0, 0);
}

Gradipcon::Gradipcon(const Gradipcon &g)
{
    Copiar(g);
}

Gradipcon::~Gradipcon ()
{
    Destruir();
}

Gradipcon &Gradipcon::operator=(const Gradipcon &g)
{
    Destruir();
    Copiar(g);
    return *this;
}


void Gradipcon::Copiar(const Gradipcon &g)
{
    int i, j;

    n = g.n;
    ne = g.ne;
    ns = g.ns;
    max = g.max;
    maxe = g.maxe;
    maxs = g.maxs;

    neu = new Neurona*[max];
    for(i = 0; i < n; i++)
    {
        neu[i] = new Neurona;
        *neu[i] = *g.neu[i];
    }
    for (i = n; i < max; i++)
        neu[i] = NULL;

    arcos = new Arco*[max + 1];	// matriz de adyacencia + pesos entradas
    for (i = 0; i < n; i++)
    {
        arcos[i] = new Arco[max];

        for (j = 0; j < max; j++)
        {
            arcos[i][j].existe = g.arcos[i][j].existe;
            arcos[i][j].peso = g.arcos[i][j].peso;
        }
    }

    for (i = n; i < max; i++)
        arcos[i] = NULL;

    arcos[max] = new Arco[max];	// fila extra

    entradas = new int [maxe];	// vectores de e/s (maximos)
    salidas = new int [maxs];

    for (i = 0; i < ne; i++)
        entradas[i] = g.entradas[i];

    for (i = 0; i < ns; i++)
        salidas[i] = g.salidas[i];

    modo = g.modo;
}

void Gradipcon::Destruir()
{
    int i;
    for (i = 0; i < n; i++)
    {
        delete neu[i];
        delete arcos[i];
    }

    delete arcos[max];

    delete[] neu;
    delete[] arcos;
    delete[] entradas;
    delete[] salidas;
}

bool Gradipcon::setEntradas (int num, int v[])
{
    if (num <= maxe)
    {
        ne = num;

        for (int i = 0; i < ne; i++)
        {
            entradas[i] = v[i];
            arcos[max][v[i]].existe = true;
            arcos[max][v[i]].peso = 1;
        }

        return true;
    }
    else
        return false;
}

bool Gradipcon::setSalidas (int num, int v[])
{
    if (num <= maxs)
    {
        ns = num;

        for (int i = 0; i < ns; i++)
            salidas[i] = v[i];

        return true;
    }
    else
        return false;
}

bool Gradipcon::setEntradas (int num)
{
    if (num <= maxe)
    {
        ne = num;

        for (int i = 0; i < ne; i++)
        {
            entradas[i] = i;
            arcos[max][i].existe = true;
            arcos[max][i].peso = 1;
        }

        return true;
    }
    else
        return false;
}

bool Gradipcon::setSalidas (int num)
{
    if (num <= maxs)
    {
        ns = num;

        for (int i = 0; i < ns; i++)
            salidas[i] = i;

        return true;
    }
    else
        return false;
}

bool Gradipcon::setArcos (int x, bool *arcos)
{
    int i, j;
    if (x == n)
    {
        for (i = 0; i < n; i++)
            for (j = 0; j < n; j++)
                this->arcos[i][j].existe = arcos[i*x + j];

        return true;
    }
    else
        return false;
}

void Gradipcon::setModo(int modo_p) {
    this->modo = modo_p;
}

/**
 *		Acumular
 *
 * *1:	a las neuronas que tienen entradas exteriores, se les añade un
 *		estimulo que es su señal por su peso, que esta guardada en la fila
 *		extra de la matriz de adyacencia.
 *
 * *2:	si una neurona i esta activa, acumulamos su señal a todas a las 
 *		que afecte, modificado por el peso de ese enlace.
 *
 *  matrices:
 *     ( 1 )             ( 2 7  4 )
 *     ( 2 ) * (2 7 4) = ( 4 14 8 )
 *     ( 3 )             ( 6 21 12)
 */
void Gradipcon::Acumular(const bool ent[]) {
    int i, j;

    switch (modo) {
        case hebbiano:
            forEachArco([this, ent](Arco &arco, const int &from, const int &to) {
                neu[to]->estimulo += pulse_values[ent[from]] * arco.peso;    // *1
            });
            break;
        case basico:
            break;
        default:
            //cout << "acumular:\n" << *neu[0] << *neu[1] << endl;
            for (i = 0; i < ne; i++) {  // entradas
                neu[entradas[i]]->estimulo += pulse_values[ent[i]] * arcos[max][i].peso;    // *1
                last_ent[i] = ent[i];
            }

            for (i = 0; i < n; i++) {   //todas
                for (j = 0; j < n; j++) {
                    if (arcos[i][j].existe) {
                        neu[j]->estimulo += pulse_values[neu[i]->pulso] * arcos[i][j].peso;
                    }
                }    // *2
            }
            //cout << *neu[0] << *neu[1] << endl;
            break;
    }
}

void Gradipcon::Activar (bool sal[])
{
    int i;

    switch (modo) {
        case afinidad:
            for (int from = 0; from < n; ++from) {
                for (int to = 0; to < n; ++to) {
                    if (arcos[from][to].existe) {
                        short delta = 0;
                        int incUmbral = 0;
                        if (neu[from]->pulso == pulse_values[1] && last_ent[to] == 1) {
                            delta = 1;
                            incUmbral = -1;
                        } else if (neu[from]->pulso == pulse_values[1] && last_ent[to] == 0) {
                            delta = -1;
                            incUmbral = 1;
                        }
                        neu[to]->umbral += incUmbral;
                        arcos[from][to].peso += delta;
                    }
                }
            }
            break;
        default:
            break;

    }
    //cout << "activar:\n" << *neu[0] << *neu[1] << endl;
    for (i = 0; i < n; i++) {
        neu[i]->pulso = (neu[i]->estimulo >= neu[i]->umbral);

        //neu[i]->estimulo = 0;
    }

    for (i = 0; i < ns; i++) {
        sal[i] = neu[salidas[i]]->pulso;
    }

    for (i = 0; i < n; i++) {
        neu[i]->last_estimulo = neu[i]->estimulo;
        neu[i]->estimulo = 0;
    }
    //cout << *neu[0] << *neu[1] << endl;
}

void Gradipcon::Reconfigurar (const bool ent[], bool sal[])
{
    int i, j;
    // primera aprox:
    /*
    const int asdf = nombres_modos["basico"];
    cout << "asdf = " << asdf << endl;
*/
    switch (modo)
    {
        case basico:
            if (ne == ns)
            {
                int activos;
                int err[ne];

                for (i = 0; i < ne; i++)
                {
                    err[i] = ent[i] - sal[i];

                    if (err[i] != 0)
                    {
                        activos = 0;
                        for (j = 0; j < n; j++)
                        {
                            if(arcos[j][salidas[i]].existe)
                            {
                                activos += neu[j]->pulso;
                                arcos[j][salidas[i]].peso += err[i] * neu[j]->pulso;
                            }
                        }
                        //arc[max][salidas[i]].p += err[i] * ent[i];
                        arcos[max][entradas[i]].peso += err[i]*ent[i];
                        activos += ent[i];
                        neu[salidas[i]]->umbral -= err[i]*(activos);	// umbrales de las salidas o de todas?
                    }
                }
            }
            break;
        case hebbiano:  // w_ij = 1/n * sum^n(bit_i * bit_j)
            for (i = 0; i < n; i++) {
                for (j = 0; j < n; j++) {
                    if (arcos[i][j].existe) {
                        arcos[i][j].peso += pulse_values[ent[i]]*pulse_values[ent[j]];
                    }
                }
            }

//            for (i = 0; i < n; i++) {
//                neu[i]->umbral++;
//            }
//            forEachArcoEntrada([] (Arco &arco, const int &to) {
//                arco.peso += 1;
//            });

//			for (i = 0; i < ne; i++)
//				arcos[max][entradas[i]].peso += ent[i] && neu[entradas[i]]->pulso? 1: -1;

            break;
        case LTP_LTD1:
            for (i = 0; i < n; i++)
            {
                neu[i]->f.Tick();
                if (neu[i]->pulso && neu[i]->f.FrecuenciaSuperada())
                {
                    cout << "para la neurona " << i << "se potencian: ";
                    for (j = 0; j < n; j++)
                        if (arcos[i][j].existe)
                        {
                            cout << j << ", ";
                            neu[i]->f.Potenciar(arcos[i][j].peso);
                        }
                    cout << endl;
                }

                if (neu[i]->pulso)
                {
                    neu[i]->f.Reset();
                    neu[i]->umbral++;
                }
                else	// por probar
                    neu[i]->umbral--;
            }

            break;
        case LTP_LTD2:
//            cout << "LTP_LTD2" << endl;
            for (i = 0; i < n; i++) {
                neu[i]->f.Tick();

                int desviacion = 0;
                if (neu[i]->pulso > 0) {
                    desviacion = neu[i]->f.Potenciar();
//                    cout << "OOO neu[i]->f.periodo = " << neu[i]->f.periodo << endl;
                } else {
//                    cout << "XXX" << endl;
                }

                desviacion = desviacion > 0 ? 1 : desviacion;   // normalizar la desviacion a {-1, 0, 1}
                desviacion = desviacion < 0 ? -1 : desviacion;

                for (j = 0; j < n; j++) {
                    if (arcos[j][i].existe) {   // entradas
                        arcos[j][i].peso += desviacion;
                    }
                }
            }
            break;
        case afinidad:
            break;
        default:
            break;
    }
}

int Gradipcon::Aprender (const bool ent[])
{
    bool sal[ns];	//en esta aprox ns ==ne
    int iter, itermax = 5000;

    Acumular (ent);
    Activar (sal);
    iter = 0;
    while(!Iguales (ent, sal, ns) && iter < itermax)
    {
#ifdef DEPURAR
        //cout << "\n iter: " << iter << endl;
        //verSalida ();
        //verPesos ();
        //verArcos ();
        //verUmbrales ();
        verLetra (sal, 5, 7);
#endif //DEPURAR
        Reconfigurar (ent, sal);
        Acumular (ent);
        Activar (sal);
        iter++;
    }
#ifdef DEPURAR
    //cout << "\n iters: " << iter << endl;
    //verSalida ();
    //verPesos ();
    //verArcos ();
    //verUmbrales ();
    verLetra (sal, 5, 7);
#endif //DEPURAR
    return iter;
}


bool Gradipcon::Iguales (const bool v1[], const bool v2[], const int tam)
{
    for (int i = 0; i < tam; i++)
        if (v1[i] != v2[i])
            return false;

    return true;
}


void Gradipcon::verSalida ()
{
    cout <<"salida (0, 1, 2, ..., " << ns << " ): ";
    for (int i = 0; i < ns; i++)
        cout << neu[salidas[i]]->pulso << " ";

    cout << endl;
}

void Gradipcon::verPesos ()
{
    int i;
    for (i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cout << "p[" << i << "][" << j << "]: " << arcos[i][j].peso << "    ";
        }
        cout << endl;
    }

    cout << "p_entradas:                    ";
    for (i = 0; i < n; i++)
    {
        cout << "p[max][" << i << "]: " << arcos[max][i].peso << "    ";
    }
    cout << endl;
}


void Gradipcon::verArcos ()
{
    int i;
    for (i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cout << "ex[" << i << "][" << j << "]: " << arcos[i][j].existe << "    ";
        }
        cout << endl;
    }

    cout << "ex_entradas:                    ";
    for (i = 0; i < n; i++)
    {
        cout << "ex[max][" << i << "]: " << arcos[max][i].existe << "    ";
    }
    cout << endl;
}

void Gradipcon::verUmbrales ()
{
    cout <<"umbrales (0, 1, 2, ..., " << n << " ): ";
    for (int i = 0; i < n; i++)
        cout << neu[i]->umbral << " ";

    cout << endl;
}

void Gradipcon::verLetra(bool sal[], int filas, int cols)
{
    int i, j;

    for (i = 0; i < filas; i++)
    {
        for (j = 0; j < cols; j++)
            cout << (sal[i*cols + j] == true? '*': '.');
        cout << endl;
    }
    cout << endl;
}

bool Gradipcon::abrirEntrada(const char * nombre, ifstream &f)
{
    f.open(nombre);
    if (!f)
    {
        LOG_ERROR("abrirEntrada: no se pudo abrir el fichero %s\n", nombre);
        return false;
    }
    else
        return true;
}


int Gradipcon::cargarEntrada(ifstream &f, bool entr[], int cuantos)
{
    char n;
    int i = 0;


    while(i < cuantos)
    {
        i++;
        f >> n;
        if (f.eof())
        {
            LOG_ERROR(" cargarEntrada: entrada leida insuficiente\n");
            return i;
        }
        entr[i-1] = n == '*';
    }

    return i;
}


int Gradipcon::guardarEntrada(const char * nombre, bool sali[], int filas, int cols)
{
    ofstream f;
    int i, j;

    f.open(nombre);

    if (!f)
    {
        LOG_ERROR("guardarEntrada: no se pudo abrir el fichero %s\n", nombre);
        return -1;
    }

    for (i = 0; i < filas; i++)
    {
        for (j = 0; j < cols; j++)
            f << (sali[i*cols + j] == true? '*': '.');
        f << endl;
    }

    f.close();

    return i;
}

void Gradipcon::forEachNeurona(function<void(Neurona &neurona, const int &index)> callback) {
    for (int i = 0; i < n; i++) {
        callback(*(neu[i]), i);
    }
}

void Gradipcon::forEachArco(function<void(Arco &arco, const int &from, const int &to)> callback) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; ++j) {
            callback(arcos[i][j], i, j);
        }
    }
}

void Gradipcon::forEachArcoEntrada(function<void(Arco &arco, const int &to)> callback) {
    for (int i = 0; i < n; i++) {
        callback(arcos[max][i], i);
    }
}

void Gradipcon::randomize(int max, int min) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<short int> dis(min, max);

    forEachArcoEntrada([&](Arco &arco, const int &to) {
        arco.peso = dis(gen);
    });

    forEachArco([&](Arco &arco, const int &from, const int &to) {
        arco.peso = dis(gen);
    });

    forEachNeurona([&](Neurona &neurona, const int &index) {
        neurona.umbral = dis(gen)*n;
        neurona.pulso = dis(gen) % 2;
        neurona.f.Reset();
    });
}

void Gradipcon::reset(short weight, short threshold) {
    forEachArcoEntrada([&](Arco &arco, const int &to) {
        arco.existe = true;
        arco.peso = weight;
    });

    forEachArco([&](Arco &arco, const int &from, const int &to) {
        arco.existe = true;
        arco.peso = weight;
    });

    forEachNeurona([&](Neurona &neurona, const int &index) {
        neurona.umbral = threshold;
        neurona.pulso = 0;
        neurona.f.Reset();
    });
}
