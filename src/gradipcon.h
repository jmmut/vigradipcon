/***********************************************************
*
* 		autor: 	Jose Miguel Mut
*
*		fecha:	2012-06-14
*
* 		archivo:	grafo.h
*
* 		proyecto:	gradipcon
*
* 		descripcion: grafo dirigido ponderado ciclico de neuronas. Se tiene que indicar qué neuronas
* 		son las que reciben entradas y/o salidas. Implementado con un vector de punteros a neurona,
* 		<i> en <neu[i]> se usa para direccionarlas/numerarlas. Aún no sé cómo propagar el error ni
* 		si procesar hacia adelante o hacia atrás.
*
*		El modelo con una matriz de adyacencia explícita presume que habrá conectividad casi
*		completa, con lo que no vale la pena un vector en cada neurona para las postsinapsis.
*
*		Tiene las variables n, ne y ns para llevar la cuenta de las neuronas activas, neuronas con
*		entradas activas y neuronas con salidas activas, respectivamente, con posibilidad de dejar
*		de usar algunas o usar mas.
*
*		La variable estatica <MAX_NEURONAS> es solo por si no se especifica un maximo. <max> se
*		puede elegir en tiempo de ejecucion y solo esta limitada por el SO.
*
*		Modo de reconfiguracion:
*		0: correccion en base al error. supervisado. no va mal del todo para secuencias de
*			patrones.
*		1:
**********************************************************/

#ifndef _GRAFO_H_
#define _GRAFO_H_

#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <functional>
#include <random>

using std::cout;
using std::endl;
#include "neurona.h"
//#include "aux.cpp"

//#define NUDFDLC				// Necesitar� Usarla Desde Fuera De La Clase.
//#define NNUDFDLC				// No Necesitar� Usarla Desde Fuera De La Clase.


struct Arco
{
    bool existe;
    short int peso;
};

class Gradipcon {
public:
    int n, ne, ns, max, maxe, maxs;
    Neurona **neu;	// vector de punteros a Neurona
    Arco **arcos;	// matriz de adyacencia. primer indice origen, segundo indice destino

    int modo;	// modo de reconfiguracion
    //#ifdef NNUDFDLC
    //static const int MAX_NEURONAS;
    //#endif//NNUDFDLC

    bool Iguales (const bool v1[], const bool v2[], const int tam);

    enum Modo {
        basico = 0,
        hebbiano,
        LTP_LTD1,
        LTP_LTD2,
        afinidad,
    };
private:
    int *entradas;	// vector de indices (en la tabla)
    int *salidas;	// vector de indices (en la tabla)
//    const vector<string> modos = {"basico", "hebbiano", "LTP_LTD"};
//    static constexpr map<string, int> nombres_modos{{{"basico", 0}, {"hebbiano", 1}, {"LTP_LTD", 2}}};
public:
    Gradipcon(int num, int ent, int sal, int max = 10);
    Gradipcon(const Gradipcon &g);
    ~Gradipcon();

    Gradipcon &operator=(const Gradipcon &);

    bool setEntradas (int num, int v[]);
    bool setSalidas (int num, int v[]);
    bool setEntradas (int num);
    bool setSalidas (int num);
    bool setArcos (int x, bool *arcos);	// arcos es bool[x*x]
    void setModo(int modo);

    bool abrirEntrada(const char * nombre, ifstream &f);
    int cargarEntrada(ifstream &f, bool ent[], int cuantos);
    int guardarEntrada(const char * nombre, bool sal[], int filas, int cols);

    void verSalida ();
    void verPesos ();
    void verArcos ();
    void verUmbrales ();
    void verLetra(bool sal[], int filas, int cols);

    void Acumular (const bool ent[]);
    void Activar (bool sal[]);
    void Reconfigurar (const bool ent[], bool sal[]);
    int Aprender (const bool ent[]);

    void forEachNeurona(function<void (Neurona &neurona, const int &index)> callback);
    void forEachArco(function<void (Arco &arco, const int &from, const int &to)> callback);
    void forEachArcoEntrada(function<void (Arco &arco, const int &to)> callback);
    void randomize(int max, int min = 0);
    void reset(short weight, short threshold);


    //#ifdef NUDFDLC
    //static const int MAX_NEURONAS;
    //#endif//NUDFDLC
private:
    void Copiar(const Gradipcon &g);
    void Destruir();

    bool *last_ent;
    int pulse_values[2] = {-1, 1};  // or {0, 1}
};

//
//#ifdef NUDFDLC
//extern const int Gradipcon::MAX_NEURONAS = 10;
//#endif//NUDFDLC
//#ifdef NNUDFDLC
//const int Gradipcon::MAX_NEURONAS = 10;
//#endif//NNUDFDLC
//
#endif
