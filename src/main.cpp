#include "utils/SignalHandler.h"
#include "Gestor.h"
void Mostrar (bool vec[], int tam);

//const int MAX_PRUEBAS = 9;
int main(int argc, char **argv) {
    SigsegvPrinter::activate(cout);
    //Gestor *(*Prueba[MAX_PRUEBAS])() = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,Prueba7,NULL};

    int i(0), c(0);

    if (argc != 2) {
        cout << "el programa necesita un numero de prueba" << endl;
        return 0;
    }

    LOG_LEVEL(LOG_DEBUG_LEVEL)

    while (argv[1][i] != 0) {    // atoi
        c = argv[1][i++] + c * 10 - 48;
    }

    //Gestor * g = *Prueba[c]();
    Gestor1 gestor;

    gestor.mainLoop();

    cout << endl;
    return 0;
}

void Mostrar (bool vec[], int tam)
{
	cout << vec[0];
	
	for (int i = 1; i < tam; i++)
	{
		cout << " " << vec[i];
	}
	cout << endl;
}

/*
void Prueba1 ()
{
	bool palabra[8] =   {1, 0, 1, 0, 1, 0, 1, 1}; 
	bool palabra2[8] =  {0, 1, 0, 1, 1, 0, 1, 1}; 
	bool palabradist[8]={1, 0, 1, 0, 1, 0, 0, 1};
	int sum = 5, pasadas = 0, iters = 0;
	
	Grafo g(8, 8, 8);
	
	if (g.setEntradas (8) && g.setSalidas (8))		// ya no es necesario: l. 65-66 grafo.cpp
	{
		
		while (sum != 0 && sum < 500)
		{
			pasadas ++;
			sum = 0;
			sum += g.Aprender (palabra);
			sum += g.Aprender (palabra2);
			iters += sum;
			//cout << sum;
			//cin >> stop;
		}
		cout << "\n\nlenguaje aprendido en " << pasadas << " pasadas y " << iters << " iteraciones" << endl;
		palabradist[0] = 0;
		palabradist[1] = 1;
		palabradist[2] = 0;
		//palabradist[3] = 1;
		//palabradist[4] = 0;
		//palabradist[5] = 1;
		//palabradist[7] = 0;
		//palabradist[6] = 0;
		
		cout << "palabra distorsionada:     ";
		Mostrar (palabradist, 8);
		
		g.Acumular (palabradist);
		g.Activar (palabradist);
		g.verSalida ();
		g.verPesos ();
		g.verUmbrales ();
		
		cout << "palabra recuperada:      ";
		Mostrar (palabradist, 8);
	}
}

void Prueba2 ()
{
	
	bool palabra[8] =   {1, 0, 1, 0, 1, 0, 1, 1}; 
	bool palabra2[8] =  {0, 1, 0, 1, 1, 0, 1, 1}; 
	bool palabradist[8]={1, 0, 1, 0, 1, 0, 1, 1};
	int sum = 5, pasadas = 0, iters = 0;
	
	Grafo g(8, 8, 8);
	
	if (g.setEntradas (8) && g.setSalidas (8))
	{
		while (sum != 0)
		{
			pasadas ++;
			sum = 0;
			sum += g.Aprender (palabra);
			sum += g.Aprender (palabra2);
			iters += sum;
		}
		cout << "\n\nlenguaje aprendido en " << pasadas << " pasadas y " << iters << " iteraciones" << endl;
		
		palabradist[0] = 0;		// modificaciones sobre la palabra original
		palabradist[1] = 1;
		palabradist[2] = 0;
		palabradist[7] = 0;
		
		cout << "aprender palabra distorsionada:     ";
		Mostrar (palabradist, 8);
		
		g.Aprender (palabradist);
		
		cin >> sum;
		sum = 1;
		pasadas = 0;
		iters = 0;
		while (sum != 0 && sum < 5000)
		{
			pasadas ++;
			sum = 0;
			sum += g.Aprender (palabra);
			sum += g.Aprender (palabra2);
			iters += sum;
		}
		
		if(sum < 5000)
			cout << "\n\nlenguaje REaprendido en " << pasadas << " pasadas y " << iters << " iteraciones" << endl;
		else
			cout << "\n\nimposible aprender\n\n";
		
		cout << "recuerdas? palabra distorsionada:     ";
		Mostrar (palabradist, 8);
		
		g.Acumular (palabradist);
		g.Activar (palabradist);
		g.verSalida ();
		g.verPesos ();
		g.verUmbrales ();
		
		cout << "palabra recuperada:      ";
		Mostrar (palabradist, 8);
	}
}

void Prueba3 ()
{
	
	bool palabra[8] =   {1, 0, 1, 0, 1, 0, 1, 1}; 
	bool palabra2[8] =  {0, 1, 0, 1, 1, 0, 1, 1}; 
	bool palabradist[8]={1, 0, 1, 0, 1, 0, 1, 1};
	int sum = 5, pasadas = 0;
	
	Grafo g(8, 8, 8);
	
	if (g.setEntradas (8) && g.setSalidas (8))
	{
		
		palabradist[0] = 0;
		palabradist[1] = 1;
		palabradist[2] = 0;
		palabradist[7] = 0;
		
		while (sum != 0 && sum < 5000)
		{
			pasadas ++;
			sum = 0;
			sum += g.Aprender (palabra);
			sum += g.Aprender (palabra2);
			sum += g.Aprender (palabradist);
		}
		
		if(sum < 5000)
			cout << "\n\nlenguaje aprendido en nº de pasadas : " << pasadas << endl;
		else
			cout << "imposible aprender";
	}
}

void Prueba4 ()
{
	
	bool palabra[8] =   {1, 0, 1, 0, 1, 0, 1, 1}; 
	bool palabra2[8] =  {0, 1, 0, 1, 1, 0, 1, 1};
	
	Grafo g(8, 8, 8);
	
	if (g.setEntradas (8) && g.setSalidas (8))
	{
		cout << "itera main : " << g.Aprender(palabra) << endl << endl;
		cout << "que responde si le pregunto algo que no ha aprendido?: ";
		Mostrar (palabra2, 8);
		g.Acumular (palabra2);
		g.Activar (palabra2);
		cout << "responde esto: ";
		g.verSalida ();
		g.verPesos ();
		g.verUmbrales ();
	}
}

void Prueba5 ()
{
	
	bool and0[3] = {0, 0, 0}, and1[3] = {0, 1, 0};
	bool and2[3] = {1, 0, 0}, and3[3] = {1, 1, 1};
	
	int sum = 5, pasadas = 0, iters = 0;
	
	Grafo g(3, 3, 3);
	
	if (g.setEntradas (3) && g.setSalidas (3))
	{

		while (sum != 0)
		{
			pasadas ++;
			sum = 0;
			sum += g.Aprender (and0);
			sum += g.Aprender (and1);
			sum += g.Aprender (and2);
			sum += g.Aprender (and3);
			iters += sum;
		}
		cout << "\n\nlenguaje aprendido en " << pasadas;
		cout << " pasadas y " << iters << " iteraciones" << endl;
	}
}

void Prueba6 ()
{
	bool x0[3] = {0, 0, 0}, x1[3] = {0, 1, 1};
	bool x2[3] = {1, 0, 1}, x3[3] = {1, 1, 0};
	bool aux[3], aux2[3] = {0, 0, 1};
	
	int sum = 5, pasadas = 0, iters = 0;
	
	Grafo g(3, 3, 3);
	
	if (g.setEntradas (3) && g.setSalidas (3))
	{
		
		while (sum != 0 && sum < 5000)
		{
			pasadas ++;
			//sum = 0;
			sum += g.Aprender (x0);
			sum += g.Aprender (x1);
			sum += g.Aprender (x2);
			sum += g.Aprender (x3);
			iters += sum;

			cin >> stop;
		}
		cout << "\n\nlenguaje aprendido en " << pasadas;
		cout << " pasadas y " << iters << " iteraciones" << endl;
		
		g.Acumular (aux2);
		g.Activar (aux);
		g.verSalida ();
		g.verPesos ();
		g.verUmbrales ();
		g.Acumular (aux2);
		g.Activar (aux);
		g.verSalida ();
		g.verPesos ();
		g.verUmbrales ();
	}
}

void Prueba7 ()
{
	abrirEntrada ("entrenamiento.txt", f);


	while (sum != 0 && sum < 5000)
	{
		pasadas ++;
		sum = 0;
		sum += g.Aprender (vec0);
		sum += g.Aprender (vec1);
		cin >> ch;
		if (ch == 's')
			break;
	}

	cout << "\n\nlenguaje aprendido en " << pasadas;
	cout << " pasadas y " << sum << " iteraciones" << endl;

	abrirEntrada ("ruido.txt", f);
	cargarEntrada (f, ruido, 35);
	f.close();
	
	g.Acumular (ruido);
	g.Activar (res);
	g.verLetra (res, 5, 7);

	g.Acumular (ruido);
	g.Activar (res);
	g.verLetra (res, 5, 7);


}
*/
