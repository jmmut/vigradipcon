/***********************************************************
 * 
 * 		autor: 	Jose Miguel Mut
 * 
 *		fecha:	2012-06-15
 *
 * 		archivo:	neurona.cpp
 * 
 * 		proyecto:	grafo neuronal
 * 
 * 		descripcion:
 * 
 **********************************************************/


#include "neurona.h"


Neurona::Neurona()
{
	/*u = new int [2];
	u[0] = 0;
	u[1] = 0;*/
	umbral = 0;
	
	//t = 0;
	estimulo = 0;
	last_estimulo = 0;
	pulso = 0;
//	f.setPeriodo(3);
}
/*
Neurona::~Neurona()
{
	delete [] p1;
	delete [] p2;
	delete [] u;
}


int Neurona::operator()(int e1,int e2)
{
	if(e1 * p1 + e2 * p2 >= u)
		return 1;
	else
		return 0;
}

bool Neurona::operator()(int e1,int e2,int s)
{
	int e = s - (*this)(e1,e2);
	u -= e;
	p1 += e * e1;
	p2 += e * e2;
	return bool(e);
}*/

const Neurona &Neurona::operator=(const Neurona &n)
{
	umbral = n.umbral;
	pulso = n.pulso;
	estimulo = n.estimulo;
	f = n.f;
	return *this;
}


void Neurona::Mostrar()
{
	cout << *this;
}

ostream& operator<< (ostream& o, const Neurona& n)
{
	o << "(" << n.estimulo << ", " << n.umbral << ", " << (n.pulso? "****": "----") << ") ";
	
	return o;
}
