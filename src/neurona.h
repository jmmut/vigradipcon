/***********************************************************
* 
* 		autor: 	Jose Miguel Mut
* 
*		fecha:	2012-06-14
*
* 		archivo:	neurona.h
* 
* 		proyecto:	grafo neuronal
* 
* 		descripcion: cada neurona solo tiene si está activa (pulso), su umbral (u) 
*			y el estimulo acumulado que lleva en este turno.
*			los vectores de los pesos estan en el grafo.
* 
**********************************************************/

#ifndef _NEURONA_H_
#define _NEURONA_H_
#include <iostream>
using namespace std;
#include "Frecuenciador.h"

class Neurona
{
	private:
	public:
		int umbral;	//umbral
		bool pulso/*, t*/;	// turno, pulso: si esta activa
		int estimulo;	// estimulo acumulado
        int last_estimulo;
		Frecuenciador f;	// para implementar LTP y LTD
		
		Neurona();
		/*
		int operator()(int e1,int e2);		//pendiente
		
		bool operator()(int e1,int e2,int s);	//pendiente
		*/
		const Neurona &operator=(const Neurona &n);
		void Mostrar();
		friend ostream& operator<< (ostream& o, const Neurona& n);
};


#endif
