#include <vector>
#include "Frecuenciador.h"

int main() {
    Frecuenciador frecuenciador;
    vector<int> signals1{ 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0};
    vector<int> signals2{0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0};
    vector<int> signals3{1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
//    frecuenciador.setPeriodo(2);
    for (int pulse : signals1) {
        frecuenciador.Tick();
        if (pulse > 0) {
            frecuenciador.Potenciar();
        }
        cout << frecuenciador.toString() << endl;
    }
}
